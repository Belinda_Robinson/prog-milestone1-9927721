﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task_22
{
    class Program
    {
        static void Main(string[] args)
        {
            var dict = new Dictionary<string, string>();

            dict.Add("Silverbeet", "Vegetable");
            dict.Add("Broccoli", "Vegetable");
            dict.Add("Apple", "Fruit");
            dict.Add("Kiwifruit", "Fruit");
            dict.Add("Pear", "Fruit");
            dict.Add("Carrot", "Vegetable");

            foreach (var x in dict)
            {
                Console.WriteLine($"There are {x} in my dictionary");
            }
                           
            
        }
    }
}
