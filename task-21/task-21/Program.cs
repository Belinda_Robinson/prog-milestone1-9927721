﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task_21
{
    class Program
    {
        static void Main(string[] args)
        {
            var months = new Dictionary<string, double>();

            months.Add("january", 31);
            months.Add("february", 28);
            months.Add("march", 31);
            months.Add("april", 30);
            months.Add("may", 31);
            months.Add("june", 30);
            months.Add("july", 31);
            months.Add("august", 31);
            months.Add("september", 30);
            months.Add("october", 31);
            months.Add("november", 30);
            months.Add("december", 31);

            foreach (KeyValuePair<string, double> x in months)
            {
                if (x.Value.Equals(31))
                {
                    Console.WriteLine(x.Key.ToString() + x.Value.ToString());
                }


            }
        }
    }
}