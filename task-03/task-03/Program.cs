﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task_03
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Pick either \"km\" or \"m\" for miles");
            var pick = (Console.ReadLine());
            const double km2m = 0.621371;
            const double m2km = 1.609344;

            if (pick == "m")
            {
                Console.WriteLine("pick a whole number to convert km to miles");
                var number = int.Parse(Console.ReadLine());
                var answer = Math.Round(km2m * number, 2);
                Console.WriteLine($"{answer} miles");

            }
            else if (pick == "km")
            {
                Console.WriteLine("pick a whole number to convert miles to km");
                var number = int.Parse(Console.ReadLine());
                var answer = Math.Round(m2km * number, 2);
                Console.WriteLine($"{answer} kilometers");

            }
        }
}
