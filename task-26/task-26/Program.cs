﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task_26
{
    class Program
    {
        static void Main(string[] args)
        {
            var colours = new string[4] { "purple", "yellow", "pink", "blue" };

            Array.Sort(colours);
            Array.Reverse(colours);

            Console.WriteLine(string.Join(",", colours));
            Console.ReadLine();
        }
    }
}
