﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task_38
{
    class Program
    {
        static void Main(string[] args)
        {
            var i = 0;
            var counter = 12;

            Console.WriteLine("Please enter a number, and the division table from 1x to 12x will be displayed.");

            var a = Console.ReadLine();
            var b = double.Parse(a);

            for (i = 0; i < counter; i++)
            {
                var c = i + 1;
                var d = (b / c);
                Console.WriteLine($"{b} / {c} = {d}");
            }
        }
    }
}
