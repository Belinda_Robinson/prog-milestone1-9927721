﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task_04
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("pick either \"f\" for fahrenheit or \"c\" for celsius");
            var pick = (Console.ReadLine());
            const double f2c = -17.2222;
            const double c2f = 33.80000;

            if (pick == "f")
            {
                Console.WriteLine("pick a whole number to convert celsius to fahrenheit");
                var number = int.Parse(Console.ReadLine());
                var answer = Math.Round(c2f * number, 2);
                Console.WriteLine($"{answer} fahrenheit");
            }

            else if (pick == "c")
            {
                Console.WriteLine("pick a whole number to convert fahrenheit to celsius");
                var number = int.Parse(Console.ReadLine());
                var answer = Math.Round(f2c * number, 2);
                Console.WriteLine($"{answer} celsius");
            }
    }
}
