﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task_25
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("enter a value");
            var input = Console.ReadLine();
            var a = 0;
            bool value = int.TryParse(input, out a);

            Console.WriteLine($"you entered {input}");
            Console.WriteLine($"was it a number? {value}");
        }
    }
}
