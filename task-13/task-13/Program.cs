﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task_13
{
    class Program
    {
        static void Main(string[] args)
        {
            double cheese = 5.10;
            double bread = 2.20;
            double milk = 4.50;

            Console.WriteLine("choose the 3 amounts of cheese, bread and milk you would like to buy");

            var a = double.Parse(Console.ReadLine());
            var b = double.Parse(Console.ReadLine());
            var c = double.Parse(Console.ReadLine());


            var answer = Math.Round(cheese * a + bread * b + milk * c * .15, 2);

            Console.WriteLine($"your total shop comes to ${answer} includes GST");
        }
    }
}
