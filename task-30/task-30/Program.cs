﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task_30
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("enter a whole number then push enter");
            var a = Console.ReadLine();
            var c = double.Parse(a);

            Console.WriteLine("enter a second whole number then push enter");
            var b = Console.ReadLine();
            var d = double.Parse(b);

            var x = (a + b);
            Console.WriteLine($"if we take string {a} and add {b} we have {x}");
            var z = (c + d);
            Console.WriteLine($"if we take number {c} and add {d} we have {z}");
        }
    }
}
