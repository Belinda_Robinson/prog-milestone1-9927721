﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task_05
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("choose a whole number to convert from 12hr to 24hr");
            var a = int.Parse(Console.ReadLine());

            if (a > 12)
            {
                Console.WriteLine((a - 12)+ "pm");
            }
            else  
            {
                Console.WriteLine((a) + "am");
            }
                                            
        }
    }
}
