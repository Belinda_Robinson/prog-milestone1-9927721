﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task_31
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("enter a number to find out if it is divisible by 3 and 4");
            var number = int.Parse(Console.ReadLine());
            var a = number % 3;
            var b = number % 4;

            if (a == 0)
            {
                if (b == 0)
                {
                    Console.WriteLine($"{number} is divisible by 3 and 4");
                }
                else
                {
                    Console.WriteLine($"{number} is not divisible by 3 and 4");
                }

            }
            else
            {
                Console.WriteLine($"{number} not dividble by 3 and 4 as a whole number");
            }
    }
}
