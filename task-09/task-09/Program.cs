﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task_09
{
    class Program
    {
        static void Main(string[] args)
        {
            var count = 2036;
            var i = 2016;

            while (i < count)
            {
                var a = i + 4;
                Console.WriteLine($"{a} is a leap year");

                i++; i++; i++; i++;
            }
        }
    }
}