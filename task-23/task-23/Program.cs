﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task_23
{
    class Program
    {
        static void Main(string[] args)
        {
            var days = new Dictionary<string, string>();

            days.Add("monday", "weekday");
            days.Add("tuesday", "weekday");
            days.Add("wednesday", "weekday");
            days.Add("thursday", "weekday");
            days.Add("friday", "weekday");
            days.Add("saturday", "weekend");
            days.Add("sunday", "weekend");

            foreach (KeyValuePair<string, string> a in days)
            {
                if (a.Value.Equals("weekday"))
                {
                    Console.WriteLine(a.Key.ToString() + " weekday");
                }
                else
                {
                    Console.WriteLine(a.Key.ToString() + " weekend");
                }
            }

        }
    }
}
